
clear
echo "*****PREPARANDO AMBIENTE"
killall rmiregistry

rm bin/*.class
#rm bin/game/*.class

echo "*****COMPILANDO"

javac src/*.java -d ./bin/

#rmic JogoImpl || /usr/java/jdk1.8.0_71/bin/rmic JogoImpl

cd bin/

echo "*****RMIC"
rmic -nowarn JogoImpl

echo "*****REGISTRANDO"

rmiregistry &
sleep 1

echo "*****EXECUTANDO"

java Servidor &
sleep 1
java Cliente

