

public class CadastraAnimal implements No {
	
	private Animal outro;

	public CadastraAnimal(Animal outro) {
		this.outro = outro;
	}

	@Override
	public No pai() {
		return null;
	}

	@Override
	public No direita() {
		return null;
	}

	@Override
	public No esquerda() {
		return null;
	}

	@Override
	public String getPergunta() {
		return "Desisto! Em qual animal você pensou?";
	}

	@Override
	public No responder(String resposta) {
		return new CadastraCaracteristica(new Animal(null, resposta), outro);
		
	}

}
