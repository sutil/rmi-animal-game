

public class Animal implements No {
	
	
	
	public Animal(No pai, String nome) {
		this.pai = pai;
		this.direita = new Sucesso();
		this.esquerda = new CadastraAnimal(this);
		this.nome = nome;
	}

	private No pai;
	private No direita;
	private No esquerda;
	private String nome;

	@Override
	public No pai() {
		return pai;
	}

	@Override
	public No direita() {
		return direita;
	}

	@Override
	public No esquerda() {
		return esquerda;
	}
	
	public String getNome() {
		return nome;
	}

	@Override
	public String getPergunta() {
		return "O animal que você pensou é o(a) "+nome+"?";	
	}

	@Override
	public No responder(String resposta) {
		if("sim".equals(resposta))
			return this.direita;
		
		return this.esquerda;
		
	}

	public void setPai(Caracteristica novaCaracteristica) {
		this.pai = novaCaracteristica;
		
	}


}
