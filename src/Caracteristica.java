
public class Caracteristica implements No {
	
	private No pai;
	private No direita;
	private No esquerda;
	private String nome;

	public Caracteristica(String nome, No pai, No direita, No esquerda) {
		this.nome = nome;
		this.pai = pai;
		this.direita = direita;
		this.esquerda = esquerda;
	}

	@Override
	public No pai() {
		return pai;
	}

	@Override
	public No direita() {
		return direita;
	}
	
	@Override
	public No esquerda() {
		return esquerda;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setDireita(No direita) {
		this.direita = direita;
	}
	
	public void setEsquerda(No esquerda) {
		this.esquerda = esquerda;
	}

	@Override
	public String getPergunta() {
		return "O animal que você pensou é "+nome+"?";
	}

	@Override
	public No responder(String resposta) {
		if("sim".equals(resposta))
			return this.direita;
		
		else
			return this.esquerda;
	}

	public void substitui(Animal outro, Caracteristica novaCaracteristica) {
		if(direita.equals(outro))
			direita = novaCaracteristica;
		else
			esquerda = novaCaracteristica;
		
	}


}
