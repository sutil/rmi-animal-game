
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

public class Cliente  {
	
	static String resposta = "";

	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
		Jogo jogo = (Jogo)Naming.lookup( "//localhost/jogo");
		
		System.out.println(jogo.start());
		System.out.println("Tecle enter para começar!");
		
		Scanner scanner = new Scanner(System.in);
		
		resposta = scanner.nextLine();
		
		while(!"fim".equals(resposta)){
			
			try {
				String pergunta = jogo.getPergunta();
				System.out.println(pergunta);
				
				resposta = scanner.nextLine();
				jogo.responder(resposta);

			} catch (RemoteException e) {
				System.out.println(e.getMessage());
			}
			
		}
		System.out.println("O jogo acabou!");
		scanner.close();
		
	}
}
