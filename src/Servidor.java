
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;

public class Servidor {
	
	Servidor(){
		try {
			Naming.rebind("//localhost/jogo", new JogoImpl());
		} catch (RemoteException | MalformedURLException e) {
			e.printStackTrace();
		} 
	}
	
	public static void main(String[] args){
		new Servidor();
	}

}
