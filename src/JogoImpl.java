
import java.rmi.RemoteException;

public class JogoImpl extends java.rmi.server.UnicastRemoteObject implements Jogo {
	
	public static No raiz;
	No atual;
	No pai;

	private static final long serialVersionUID = 1L;

	protected JogoImpl() throws RemoteException {
		super();
	}

	@Override
	public String start() throws java.rmi.RemoteException {
		Caracteristica caracteristica = new Caracteristica("vive na água", null, null, null);
		Animal tubarao = new Animal(caracteristica, "tubarão");
		Animal macaco = new Animal(caracteristica, "macaco");
		
		caracteristica.setDireita(tubarao);
		caracteristica.setEsquerda(macaco);
		
		
		raiz = new Raiz(caracteristica);
		atual = raiz;
		pai = atual;
		return "Responda somente com 'sim', 'nao' e 'fim'.";
	}

	@Override
	public String getPergunta() throws RemoteException {
		return atual.getPergunta();
	}
	
	@Override
	public void responder(String resposta) throws RemoteException {
		if(!"fim".equals(resposta))
			atual = atual.responder(resposta);
		
	}

	@Override
	public void setNovoAnimal(String novoAnimal, String caracteristca) throws RemoteException {
		
	}

	public void restart() throws RemoteException {
		atual = pai;
	}
	
}
