

public interface No {
	
	No pai();
	No direita();
	No esquerda();
	String getPergunta();
	No responder(String resposta);

}
