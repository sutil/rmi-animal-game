

public class Sucesso implements No {

	@Override
	public No pai() {
		return null;
	}

	@Override
	public No direita() {
		return null;
	}

	@Override
	public No esquerda() {
		return null;
	}

	@Override
	public String getPergunta() {
		return "Acertei! Digite 'fim' para encerrar ou tecle <enter> para iniciar!";
	}

	@Override
	public No responder(String resposta) {
		return JogoImpl.raiz;
	}

}
