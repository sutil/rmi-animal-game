

public class CadastraCaracteristica implements No{

	Animal novo;
	Animal outro;
	
	public CadastraCaracteristica(Animal animal, Animal outro) {
		this.outro = outro;
		this.novo = animal;
	}

	@Override
	public No pai() {
		return null;
	}

	@Override
	public No direita() {
		return null;
	}

	@Override
	public No esquerda() {
		return null;
	}

	@Override
	public String getPergunta() {
		return "O que o "+novo.getNome() +" faz que o "+outro.getNome()+" não faz?";
	}

	@Override
	public No responder(String resposta) {
		
		Caracteristica caracteristicaAntiga = (Caracteristica)outro.pai();
		Caracteristica novaCaracteristica = new Caracteristica(resposta, caracteristicaAntiga, novo, outro);
		caracteristicaAntiga.substitui(outro, novaCaracteristica);
		outro.setPai(novaCaracteristica);
		novo.setPai(novaCaracteristica);
		return JogoImpl.raiz;
	}

}
