

public interface Jogo extends java.rmi.Remote {
	
	String start() throws java.rmi.RemoteException;

	String getPergunta() throws java.rmi.RemoteException;
	
	void responder(String resposta) throws java.rmi.RemoteException;

	void setNovoAnimal(String novoAnimal, String caracteristca) throws java.rmi.RemoteException;

	void restart() throws java.rmi.RemoteException;


}
