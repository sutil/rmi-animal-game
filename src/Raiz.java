
public class Raiz  implements No {

	Caracteristica caracteristica;
	
	public Raiz(Caracteristica caracteristica) {
		this.caracteristica = caracteristica;
	}
	
	@Override
	public No pai() {
		return null;
	}

	@Override
	public No direita() {
		return caracteristica;
	}

	@Override
	public No esquerda() {
		return caracteristica;
	}

	@Override
	public String getPergunta() {
		return "Pense em um animal! Pensou??";
	}

	@Override
	public No responder(String resposta) {
		if("sim".equals(resposta))
			return caracteristica;
		return this;
	}

}
